DROP TRIGGER IF EXISTS tri_act_general ON tab_pmtros;
DROP TRIGGER IF EXISTS tri_act_general ON tab_roles;
DROP TRIGGER IF EXISTS tri_act_general ON tab_info_empresa;
DROP TRIGGER IF EXISTS tri_act_general ON tab_bancos_cuentas;
DROP TRIGGER IF EXISTS tri_act_general ON tab_paises;
DROP TRIGGER IF EXISTS tri_act_general ON tab_departamentos;
DROP TRIGGER IF EXISTS tri_act_general ON tab_ciudades;
DROP TRIGGER IF EXISTS tri_act_general ON tab_zonas;
DROP TRIGGER IF EXISTS tri_act_general ON tab_barrios;
DROP TRIGGER IF EXISTS tri_act_general ON tab_direcciones;
DROP TRIGGER IF EXISTS tri_act_general ON tab_meses;
DROP TRIGGER IF EXISTS tri_act_general ON tab_festivos;
DROP TRIGGER IF EXISTS tri_act_general ON tab_novedades;
DROP TRIGGER IF EXISTS tri_act_general ON tab_motivos;
DROP TRIGGER IF EXISTS tri_act_general ON tab_eventos;
DROP TRIGGER IF EXISTS tri_act_general ON tab_alertas;
DROP TRIGGER IF EXISTS tri_act_general ON tab_estados;
DROP TRIGGER IF EXISTS tri_act_general ON tab_marcas;
DROP TRIGGER IF EXISTS tri_act_general ON tab_lineas;
DROP TRIGGER IF EXISTS tri_act_general ON tab_sublineas;
DROP TRIGGER IF EXISTS tri_act_general ON tab_productos;
DROP TRIGGER IF EXISTS tri_act_general ON tab_tipos_hospedajes;
DROP TRIGGER IF EXISTS tri_act_general ON tab_hospedajes;
DROP TRIGGER IF EXISTS tri_act_general ON tab_hospedajes_listas_precios;
DROP TRIGGER IF EXISTS tri_act_general ON tab_hospedajes_horarios;
DROP TRIGGER IF EXISTS tri_act_general ON tab_hospedajes_usuarios;
DROP TRIGGER IF EXISTS tri_act_general ON tab_tipos_habitaciones;
DROP TRIGGER IF EXISTS tri_act_general ON tab_habitaciones;
DROP TRIGGER IF EXISTS tri_act_general ON tab_tipos_servicios;
DROP TRIGGER IF EXISTS tri_act_general ON tab_servicios;
DROP TRIGGER IF EXISTS tri_act_general ON tab_tipos_caracteristicas;
DROP TRIGGER IF EXISTS tri_act_general ON tab_caracteristicas;
DROP TRIGGER IF EXISTS tri_act_general ON tab_hospedajes_habitaciones;
DROP TRIGGER IF EXISTS tri_act_general ON tab_hospedajes_servicios;
DROP TRIGGER IF EXISTS tri_act_general ON tab_hospedajes_caracteristicas;
DROP TRIGGER IF EXISTS tri_act_general ON tab_habitaciones_servicios;
DROP TRIGGER IF EXISTS tri_act_general ON tab_habitaciones_caracteristicas;
DROP TRIGGER IF EXISTS tri_act_general ON tab_clientes;
DROP TRIGGER IF EXISTS tri_act_general ON tab_paquetes;
DROP TRIGGER IF EXISTS tri_act_general ON tab_paquetes_caracteristicas;
DROP TRIGGER IF EXISTS tri_act_general ON tab_paquetes_servicios;
DROP TRIGGER IF EXISTS tri_act_general ON tab_paquetes_productos;
DROP TRIGGER IF EXISTS tri_act_general ON tab_paquetes_habitaciones;
DROP TRIGGER IF EXISTS tri_act_general ON tab_head_promociones;
DROP TRIGGER IF EXISTS tri_act_general ON tab_deta_promociones;
DROP TRIGGER IF EXISTS tri_act_general ON tab_hospedajes_promociones;
DROP TRIGGER IF EXISTS tri_act_general ON tab_head_reservacion;
DROP TRIGGER IF EXISTS tri_act_general ON tab_deta_productos_reservacion;
DROP TRIGGER IF EXISTS tri_act_general ON tab_deta_servicios_reservacion;
DROP TRIGGER IF EXISTS tri_act_general ON tab_deta_caracteristicas_reservacion;
DROP TRIGGER IF EXISTS tri_act_general ON tab_head_factura;
DROP TRIGGER IF EXISTS tri_act_general ON tab_deta_factura;
DROP TRIGGER IF EXISTS tri_act_general ON tab_reportes;
DROP TRIGGER IF EXISTS tri_act_general ON tab_head_mensajes;
DROP TRIGGER IF EXISTS tri_act_general ON tab_deta_mensajes;
DROP TRIGGER IF EXISTS tri_act_general ON tab_monitoreo;
DROP TRIGGER IF EXISTS tri_act_general ON tab_auditoria_registros;
DROP TRIGGER IF EXISTS tri_act_general ON tab_auditoria_borrado;
DROP TRIGGER IF EXISTS tri_act_general ON tab_facturacion;
DROP TRIGGER IF EXISTS tri_act_general ON tab_temporal;

DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_pmtros;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_roles;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_info_empresa;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_bancos_cuentas;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_paises;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_departamentos;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_ciudades;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_zonas;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_barrios;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_direcciones;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_meses;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_festivos;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_novedades;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_motivos;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_eventos;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_alertas;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_estados;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_marcas;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_lineas;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_sublineas;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_productos;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_tipos_hospedajes;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_hospedajes;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_hospedajes_listas_precios;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_hospedajes_horarios;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_hospedajes_usuarios;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_tipos_habitaciones;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_habitaciones;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_tipos_servicios;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_servicios;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_tipos_caracteristicas;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_caracteristicas;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_clientes;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_paquetes;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_paquetes_caracteristicas;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_paquetes_servicios;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_paquetes_productos;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_paquetes_habitaciones;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_head_promociones;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_deta_promociones;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_hospedajes_promociones;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_head_reservacion;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_deta_productos_reservacion;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_deta_servicios_reservacion;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_deta_caracteristicas_reservacion;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_head_factura;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_deta_factura;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_reportes;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_head_mensajes;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_deta_mensajes;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_monitoreo;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_auditoria_registros;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_auditoria_borrado;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_facturacion;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_temporal;

DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_hospedajes_habitaciones;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_hospedajes_servicios;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_hospedajes_caracteristicas;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_habitaciones_servicios;
DROP TRIGGER IF EXISTS tri_gen_general_codigos_registros ON tab_habitaciones_caracteristicas;

DROP TRIGGER IF EXISTS tri_gen_encrypted_passwd ON tab_hospedajes_usuarios;
DROP TRIGGER IF EXISTS tri_gen_encrypted_passwd ON tab_clientes;

DROP FUNCTION IF EXISTS fun_act_general();
DROP FUNCTION IF EXISTS fun_gen_general_codigos_registros();
DROP FUNCTION IF EXISTS fun_gen_encrypted_passwd();

DROP TABLE IF EXISTS tab_temporal;
DROP TABLE IF EXISTS tab_facturacion;
DROP TABLE IF EXISTS tab_auditoria_borrado;
DROP TABLE IF EXISTS tab_auditoria_registros;
DROP TABLE IF EXISTS tab_monitoreo;
DROP TABLE IF EXISTS tab_deta_mensajes;
DROP TABLE IF EXISTS tab_head_mensajes;
DROP TABLE IF EXISTS tab_reportes;
DROP TABLE IF EXISTS tab_deta_factura;
DROP TABLE IF EXISTS tab_head_factura;
DROP TABLE IF EXISTS tab_deta_caracteristicas_reservacion;
DROP TABLE IF EXISTS tab_deta_servicios_reservacion;
DROP TABLE IF EXISTS tab_deta_productos_reservacion;
DROP TABLE IF EXISTS tab_head_reservacion;
DROP TABLE IF EXISTS tab_hospedajes_promociones;
DROP TABLE IF EXISTS tab_deta_promociones;
DROP TABLE IF EXISTS tab_head_promociones;
DROP TABLE IF EXISTS tab_paquetes_habitaciones;
DROP TABLE IF EXISTS tab_paquetes_productos;
DROP TABLE IF EXISTS tab_paquetes_servicios;
DROP TABLE IF EXISTS tab_paquetes_caracteristicas;
DROP TABLE IF EXISTS tab_paquetes;
DROP TABLE IF EXISTS tab_clientes;
DROP TABLE IF EXISTS tab_habitaciones_caracteristicas;
DROP TABLE IF EXISTS tab_habitaciones_servicios;
DROP TABLE IF EXISTS tab_hospedajes_caracteristicas;
DROP TABLE IF EXISTS tab_hospedajes_servicios;
DROP TABLE IF EXISTS tab_hospedajes_habitaciones;
DROP TABLE IF EXISTS tab_caracteristicas;
DROP TABLE IF EXISTS tab_tipos_caracteristicas;
DROP TABLE IF EXISTS tab_servicios;
DROP TABLE IF EXISTS tab_tipos_servicios;
DROP TABLE IF EXISTS tab_habitaciones;
DROP TABLE IF EXISTS tab_tipos_habitaciones;
DROP TABLE IF EXISTS tab_hospedajes_usuarios;
DROP TABLE IF EXISTS tab_hospedajes_horarios;
DROP TABLE IF EXISTS tab_hospedajes_listas_precios;
DROP TABLE IF EXISTS tab_hospedajes;
DROP TABLE IF EXISTS tab_tipos_hospedajes;
DROP TABLE IF EXISTS tab_productos;
DROP TABLE IF EXISTS tab_sublineas;
DROP TABLE IF EXISTS tab_lineas;
DROP TABLE IF EXISTS tab_marcas;
DROP TABLE IF EXISTS tab_estados;
DROP TABLE IF EXISTS tab_alertas;
DROP TABLE IF EXISTS tab_eventos;
DROP TABLE IF EXISTS tab_motivos;
DROP TABLE IF EXISTS tab_novedades;
DROP TABLE IF EXISTS tab_festivos;
DROP TABLE IF EXISTS tab_meses;
DROP TABLE IF EXISTS tab_direcciones;
DROP TABLE IF EXISTS tab_barrios;
DROP TABLE IF EXISTS tab_zonas;
DROP TABLE IF EXISTS tab_ciudades;
DROP TABLE IF EXISTS tab_departamentos;
DROP TABLE IF EXISTS tab_paises;
DROP TABLE IF EXISTS tab_bancos_cuentas;
DROP TABLE IF EXISTS tab_info_empresa;
DROP TABLE IF EXISTS tab_roles;
DROP TABLE IF EXISTS tab_pmtros;

CREATE TABLE tab_pmtros(
-- ********************     ********************************************      *********
   id_pmtro                 SERIAL                                            NOT NULL,
   cod_pmtro                VARCHAR                                           NOT NULL,
   va_cargo_fijo_mensual    DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   va_cargo_procentaje      DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   nom_alerta_suave         VARCHAR DEFAULT 'no_sonido.mp3'                   NOT NULL,
   nom_alerta_media         VARCHAR DEFAULT 'no_sonido.mp3'                   NOT NULL,
   nom_alerta_fuerte        VARCHAR DEFAULT 'no_sonido.mp3'                   NOT NULL,
   fec_primer_corte         INTEGER DEFAULT 10                                NOT NULL,
   fec_segundo_corte        INTEGER DEFAULT 25                                NOT NULL,
   val_porc_iva             DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_pmtro)
);
CREATE TABLE tab_roles(
-- ********************     ********************************************      *********
   id_role                  SERIAL                                            NOT NULL,
   cod_role                 VARCHAR                                           NOT NULL,
   nom_role                 VARCHAR                                           NOT NULL,
   nivel_privilegio         INTEGER DEFAULT 1                                 NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_role)
);
CREATE TABLE tab_info_empresa(
-- ********************     ********************************************      *********
   id_empresa               SERIAL                                            NOT NULL,
   cod_empresa              VARCHAR                                           NOT NULL,
   nit_empresa              VARCHAR                                           NOT NULL,
   nom_empresa              VARCHAR DEFAULT 'MovilidadApps S.A.S.'            NOT NULL,
   tel_empresa              VARCHAR                                           NOT NULL,
   dir_empresa              VARCHAR                                           NOT NULL,
   mail_empresa             VARCHAR                                           NOT NULL,
   site_empresa             VARCHAR                                           NOT NULL,
   fec_aniversario          DATE DEFAULT '2014-06-14'                         NOT NULL,
   nom_contacto_adm         VARCHAR                                           NOT NULL,
   tel_contacto_adm         VARCHAR                                           NOT NULL,
   cel_contacto_adm         VARCHAR                                           NOT NULL,
   nom_contacto_cli         VARCHAR                                           NOT NULL,
   tel_contacto_cli         VARCHAR                                           NOT NULL,
   cel_contacto_cli         VARCHAR                                           NOT NULL,
   nom_contacto_tec         VARCHAR                                           NOT NULL,
   tel_contacto_tec         VARCHAR                                           NOT NULL,
   cel_contacto_tec         VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_empresa)
);
CREATE TABLE tab_bancos_cuentas(
-- ********************     ********************************************      *********
   id_cuenta                SERIAL                                            NOT NULL,
   cod_cuenta               VARCHAR                                           NOT NULL,
   nom_banco                VARCHAR                                           NOT NULL,
   num_cuenta               VARCHAR                                           NOT NULL,
   ind_tipo_cuenta          INTEGER DEFAULT 1                                 NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_cuenta)
);
CREATE TABLE tab_paises(
-- ********************     ********************************************      *********
   id_pais                  SERIAL                                            NOT NULL,
   cod_pais                 VARCHAR                                           NOT NULL,
   nom_pais                 VARCHAR                                           NOT NULL,
   cod_telefono             VARCHAR                                           NOT NULL,
   val_latitud_medio        DECIMAL(10,6) DEFAULT 0.0                         NOT NULL,
   val_longitud_medio       DECIMAL(10,6) DEFAULT 0.0                         NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_pais)
);
CREATE TABLE tab_departamentos(
-- ********************     ********************************************      *********
   id_pais                  BIGINT                                            NOT NULL,
   id_departamento          SERIAL                                            NOT NULL,
   cod_departamento         VARCHAR                                           NOT NULL,
   nom_departamento         VARCHAR                                           NOT NULL,
   cod_telefono             VARCHAR                                           NOT NULL,
   val_latitud_medio        DECIMAL(10,6) DEFAULT 0.0                         NOT NULL,
   val_longitud_medio       DECIMAL(10,6) DEFAULT 0.0                         NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_departamento),
  FOREIGN KEY(id_pais) REFERENCES tab_paises(id_pais)
);
CREATE TABLE tab_ciudades(
-- ********************     ********************************************      *********
   id_pais                  BIGINT                                            NOT NULL,
   id_departamento          BIGINT                                            NOT NULL,
   id_ciudad                SERIAL                                            NOT NULL,
   cod_ciudad               VARCHAR                                           NOT NULL,
   nom_ciudad               VARCHAR                                           NOT NULL,
   cod_telefono             VARCHAR                                           NOT NULL,
   val_latitud_medio        DECIMAL(10,6) DEFAULT 0.0                         NOT NULL,
   val_longitud_medio       DECIMAL(10,6) DEFAULT 0.0                         NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_ciudad),
  FOREIGN KEY(id_pais) REFERENCES tab_paises(id_pais),
  FOREIGN KEY(id_departamento) REFERENCES tab_departamentos(id_departamento)
);
CREATE TABLE tab_zonas(
-- ********************     ********************************************      *********
   id_pais                  BIGINT                                            NOT NULL,
   id_departamento          BIGINT                                            NOT NULL,
   id_ciudad                BIGINT                                            NOT NULL,
   id_zona                  SERIAL                                            NOT NULL,
   cod_zona                 VARCHAR                                           NOT NULL,
   nom_zona                 VARCHAR                                           NOT NULL,
   val_latitud_medio        DECIMAL(10,6) DEFAULT 0.0                         NOT NULL,
   val_longitud_medio       DECIMAL(10,6) DEFAULT 0.0                         NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_zona),
  FOREIGN KEY(id_pais) REFERENCES tab_paises(id_pais),
  FOREIGN KEY(id_departamento) REFERENCES tab_departamentos(id_departamento),
  FOREIGN KEY(id_ciudad) REFERENCES tab_ciudades(id_ciudad)
);
CREATE TABLE tab_barrios(
-- ********************     ********************************************      *********
   id_pais                  BIGINT                                            NOT NULL,
   id_departamento          BIGINT                                            NOT NULL,
   id_ciudad                BIGINT                                            NOT NULL,
   id_zona                  BIGINT                                            NOT NULL,
   id_barrio                SERIAL                                            NOT NULL,
   cod_barrio               VARCHAR                                           NOT NULL,
   nom_barrio               VARCHAR                                           NOT NULL,
   val_latitud_medio        DECIMAL(10,6) DEFAULT 0.0                         NOT NULL,
   val_longitud_medio       DECIMAL(10,6) DEFAULT 0.0                         NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_barrio),
  FOREIGN KEY(id_pais) REFERENCES tab_paises(id_pais),
  FOREIGN KEY(id_departamento) REFERENCES tab_departamentos(id_departamento),
  FOREIGN KEY(id_ciudad) REFERENCES tab_ciudades(id_ciudad),
  FOREIGN KEY(id_zona) REFERENCES tab_zonas(id_zona)
);
CREATE TABLE tab_direcciones(
-- ********************     ********************************************      *********
   id_direccion             SERIAL                                            NOT NULL,
   cod_direccion            VARCHAR                                           NOT NULL,
   nom_direccion            VARCHAR                                           NOT NULL,
   val_latitud_medio        DECIMAL(10,6) DEFAULT 0.0                         NOT NULL,
   val_longitud_medio       DECIMAL(10,6) DEFAULT 0.0                         NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_direccion)
);
CREATE TABLE tab_meses(
-- ********************     ********************************************      *********
   id_mes                   SERIAL                                            NOT NULL,
   cod_mes                  VARCHAR                                           NOT NULL,
   nom_mes                  VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_mes)
);
CREATE TABLE tab_festivos(
-- ********************     ********************************************      *********
   id_mes                   BIGINT                                            NOT NULL,
   id_festivo               SERIAL                                            NOT NULL,
   cod_festivo              VARCHAR                                           NOT NULL,
   nom_festivo              VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_festivo),
  FOREIGN KEY(id_mes) REFERENCES tab_meses(id_mes)
);
CREATE TABLE tab_novedades(
-- ********************     ********************************************      *********
   id_novedad               SERIAL                                            NOT NULL,
   cod_novedad              VARCHAR                                           NOT NULL,
   nom_novedad              VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_novedad)
);
CREATE TABLE tab_motivos(
-- ********************     ********************************************      *********
   id_motivo                SERIAL                                            NOT NULL,
   cod_motivo               VARCHAR                                           NOT NULL,
   nom_motivo               VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_motivo)
);
CREATE TABLE tab_eventos(
-- ********************     ********************************************      *********
   id_evento                SERIAL                                            NOT NULL,
   cod_evento               VARCHAR                                           NOT NULL,
   nom_evento               VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_evento)
);
CREATE TABLE tab_alertas(
-- ********************     ********************************************      *********
   id_alerta                SERIAL                                            NOT NULL,
   cod_alerta               VARCHAR                                           NOT NULL,
   nom_alerta               VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_alerta)
);
CREATE TABLE tab_estados(
-- ********************     ********************************************      *********
   id_estado                SERIAL                                            NOT NULL,
   cod_estado               VARCHAR                                           NOT NULL,
   nom_estado               VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_estado)
);
CREATE TABLE tab_marcas(
-- ********************     ********************************************      *********
   id_marca                 SERIAL                                            NOT NULL,
   cod_marca                VARCHAR                                           NOT NULL,
   nom_marca                VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_marca)
);
CREATE TABLE tab_lineas(
-- ********************     ********************************************      *********
   id_linea                 SERIAL                                            NOT NULL,
   cod_linea                VARCHAR                                           NOT NULL,
   nom_linea                VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_linea)
);
CREATE TABLE tab_sublineas(
-- ********************     ********************************************      *********
   id_linea                 BIGINT                                            NOT NULL,
   id_sublinea              SERIAL                                            NOT NULL,
   cod_sublinea             VARCHAR                                           NOT NULL,
   nom_sublinea             VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_sublinea),
  FOREIGN KEY(id_linea) REFERENCES tab_lineas(id_linea)
);
CREATE TABLE tab_productos(
-- ********************     ********************************************      *********
   id_producto              SERIAL                                            NOT NULL,
   cod_producto             VARCHAR                                           NOT NULL,
   nom_producto             VARCHAR                                           NOT NULL,
   id_marca                 BIGINT                                            NOT NULL,
   id_linea                 BIGINT                                            NOT NULL,
   ind_sublinea             BOOLEAN DEFAULT FALSE                             NOT NULL,
   id_sublinea              BIGINT,
   val_compra               DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   desc_producto            VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_producto),
  FOREIGN KEY(id_marca) REFERENCES tab_marcas(id_marca),
  FOREIGN KEY(id_linea) REFERENCES tab_lineas(id_linea),
  FOREIGN KEY(id_sublinea) REFERENCES tab_sublineas(id_sublinea)
);
CREATE TABLE tab_tipos_hospedajes(
-- ********************     ********************************************      *********
   id_tipo_hospedaje        SERIAL                                            NOT NULL,
   cod_tipo_hospedaje       VARCHAR                                           NOT NULL,
   nom_tipo_hospedaje       VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_tipo_hospedaje)
);
CREATE TABLE tab_hospedajes(
-- ********************     ********************************************      *********
   id_pais                  BIGINT                                            NOT NULL,
   id_departamento          BIGINT                                            NOT NULL,
   id_ciudad                BIGINT                                            NOT NULL,
   id_zona                  BIGINT                                            NOT NULL,
   id_barrio                BIGINT                                            NOT NULL,
   id_hospedaje             SERIAL                                            NOT NULL,
   cod_hospedaje            VARCHAR                                           NOT NULL,
   nom_hospedaje            VARCHAR                                           NOT NULL,
   nit_hospedaje            VARCHAR                                           NOT NULL,
   id_tipo_hospedaje        BIGINT                                            NOT NULL,
   tel_hospedaje            VARCHAR                                           NOT NULL,
   cel_hospedaje            VARCHAR                                           NOT NULL,
   dir_hospedaje            VARCHAR                                           NOT NULL,
   mail_hospedaje           VARCHAR                                           NOT NULL,
   site_hospedaje           VARCHAR                                           NOT NULL,
   fec_aniversario          DATE DEFAULT 'today'                              NOT NULL,
   num_habitaciones         INTEGER DEFAULT 0                                 NOT NULL,
   nom_contacto_adm         VARCHAR                                           NOT NULL,
   tel_contacto_adm         VARCHAR                                           NOT NULL,
   cel_contacto_adm         VARCHAR                                           NOT NULL,
   nom_banco                VARCHAR                                           NOT NULL,
   num_cuenta               VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_hospedaje),
  FOREIGN KEY(id_pais) REFERENCES tab_paises(id_pais),
  FOREIGN KEY(id_departamento) REFERENCES tab_departamentos(id_departamento),
  FOREIGN KEY(id_ciudad) REFERENCES tab_ciudades(id_ciudad),
  FOREIGN KEY(id_zona) REFERENCES tab_zonas(id_zona),
  FOREIGN KEY(id_barrio) REFERENCES tab_barrios(id_barrio),
  FOREIGN KEY(id_tipo_hospedaje) REFERENCES tab_tipos_hospedajes(id_tipo_hospedaje)
);
CREATE TABLE tab_hospedajes_listas_precios(
-- ********************     ********************************************      *********
   id_hospedaje             BIGINT                                            NOT NULL,
   id_producto              BIGINT                                            NOT NULL,
   id_lista_precio          SERIAL                                            NOT NULL,
   cod_lista_precio         VARCHAR                                           NOT NULL,
   val_lista_precio         VARCHAR                                           NOT NULL,
   ind_maneja_iva           BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_iva                  DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_lista_precio),
  FOREIGN KEY(id_hospedaje) REFERENCES tab_hospedajes(id_hospedaje),
  FOREIGN KEY(id_producto) REFERENCES tab_productos(id_producto)
);
CREATE TABLE tab_hospedajes_horarios(
-- ********************     ********************************************      *********
   id_hospedaje             BIGINT                                            NOT NULL,
   id_horario               SERIAL                                            NOT NULL,
   cod_horario              VARCHAR                                           NOT NULL,
   dia_semana               INTEGER                                           NOT NULL,
   hora_inicio              TIME                                              NOT NULL,
   hora_final               TIME                                              NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_horario),
  FOREIGN KEY(id_hospedaje) REFERENCES tab_hospedajes(id_hospedaje)
);
CREATE TABLE tab_hospedajes_usuarios(
-- ********************     ********************************************      *********
   id_hospedaje             BIGINT                                            NOT NULL,
   id_usuario               SERIAL                                            NOT NULL,
   cod_usuario              VARCHAR                                           NOT NULL,
   nom_usuario              VARCHAR                                           NOT NULL,
   val_login                VARCHAR                                           NOT NULL,
   val_passwd               VARCHAR                                           NOT NULL,
   val_passwd_encrypted     VARCHAR                                           NOT NULL,
   id_role                  BIGINT                                            NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_usuario),
  FOREIGN KEY(id_hospedaje) REFERENCES tab_hospedajes(id_hospedaje),
  FOREIGN KEY(id_role) REFERENCES tab_roles(id_role)
);
CREATE TABLE tab_tipos_habitaciones(
-- ********************     ********************************************      *********
   id_tipo_habitacion       SERIAL                                            NOT NULL,
   cod_tipo_habitacion      VARCHAR                                           NOT NULL,
   nom_tipo_habitacion      VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_tipo_habitacion)
);
CREATE TABLE tab_habitaciones(
-- ********************     ********************************************      *********
   id_tipo_habitacion       BIGINT                                            NOT NULL,
   id_habitacion            SERIAL                                            NOT NULL,
   cod_habitacion           VARCHAR                                           NOT NULL,
   nom_habitacion           VARCHAR                                           NOT NULL,
   desc_habitacion          VARCHAR                                           NOT NULL,
   num_aprox_personas       INTEGER                                           NOT NULL,
   val_habitacion           DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   val_noche_habitacion     DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   val_persona_habitacion   DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   val_extra_hora           DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   val_extra_persona        DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_habitacion),
  FOREIGN KEY(id_tipo_habitacion) REFERENCES tab_tipos_habitaciones(id_tipo_habitacion)
);
CREATE TABLE tab_tipos_servicios(
-- ********************     ********************************************      *********
   id_tipo_servicio         SERIAL                                            NOT NULL,
   cod_tipo_servicio        VARCHAR                                           NOT NULL,
   nom_tipo_servicio        VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_tipo_servicio)
);
CREATE TABLE tab_servicios(
-- ********************     ********************************************      *********
   id_tipo_servicio         BIGINT                                            NOT NULL,
   id_servicio              SERIAL                                            NOT NULL,
   cod_servicio             VARCHAR                                           NOT NULL,
   nom_servicio             VARCHAR                                           NOT NULL,
   ind_maneja_precio        BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_servicio             DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   desc_servicio            VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_servicio),
  FOREIGN KEY(id_tipo_servicio) REFERENCES tab_tipos_servicios(id_tipo_servicio)
);
CREATE TABLE tab_tipos_caracteristicas(
-- ********************     ********************************************      *********
   id_tipo_caracteristica   SERIAL                                            NOT NULL,
   cod_tipo_caracteristica  VARCHAR                                           NOT NULL,
   nom_tipo_caracteristica  VARCHAR                                           NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_tipo_caracteristica)
);
CREATE TABLE tab_caracteristicas(
-- ********************     ********************************************      *********
   id_tipo_caracteristica   BIGINT                                            NOT NULL,
   id_caracteristica        SERIAL                                            NOT NULL,
   cod_caracteristica       VARCHAR                                           NOT NULL,
   nom_caracteristica       VARCHAR                                           NOT NULL,
   desc_caracteristica      VARCHAR                                           NOT NULL,
   ind_maneja_precio        BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_caracteristica       DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_caracteristica),
  FOREIGN KEY(id_tipo_caracteristica) REFERENCES tab_tipos_caracteristicas(id_tipo_caracteristica)
);
CREATE TABLE tab_hospedajes_habitaciones(
-- ********************     ********************************************      *********
   id_hospedaje             BIGINT                                            NOT NULL,
   id_habitacion            BIGINT                                            NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_hospedaje, id_habitacion),
  FOREIGN KEY(id_hospedaje) REFERENCES tab_hospedajes(id_hospedaje),
  FOREIGN KEY(id_habitacion) REFERENCES tab_habitaciones(id_habitacion)
);
CREATE TABLE tab_hospedajes_servicios(
-- ********************     ********************************************      *********
   id_hospedaje             BIGINT                                            NOT NULL,
   id_servicio              BIGINT                                            NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_hospedaje, id_servicio),
  FOREIGN KEY(id_hospedaje) REFERENCES tab_hospedajes(id_hospedaje),
  FOREIGN KEY(id_servicio) REFERENCES tab_servicios(id_servicio)
);
CREATE TABLE tab_hospedajes_caracteristicas(
-- ********************     ********************************************      *********
   id_hospedaje             BIGINT                                            NOT NULL,
   id_caracteristica        BIGINT                                            NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_hospedaje, id_caracteristica),
  FOREIGN KEY(id_hospedaje) REFERENCES tab_hospedajes(id_hospedaje),
  FOREIGN KEY(id_caracteristica) REFERENCES tab_caracteristicas(id_caracteristica)
);
CREATE TABLE tab_habitaciones_servicios(
-- ********************     ********************************************      *********
   id_habitacion            BIGINT                                            NOT NULL,
   id_servicio              BIGINT                                            NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_habitacion, id_servicio),
  FOREIGN KEY(id_habitacion) REFERENCES tab_habitaciones(id_habitacion),
  FOREIGN KEY(id_servicio) REFERENCES tab_servicios(id_servicio)
);
CREATE TABLE tab_habitaciones_caracteristicas(
-- ********************     ********************************************      *********
   id_habitacion            BIGINT                                            NOT NULL,
   id_caracteristica        BIGINT                                            NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_habitacion, id_caracteristica),
  FOREIGN KEY(id_habitacion) REFERENCES tab_habitaciones(id_habitacion),
  FOREIGN KEY(id_caracteristica) REFERENCES tab_caracteristicas(id_caracteristica)
);
CREATE TABLE tab_clientes(
-- ********************     ********************************************      *********
   id_pais                  BIGINT                                            NOT NULL,
   id_departamento          BIGINT                                            NOT NULL,
   id_ciudad                BIGINT                                            NOT NULL,
   id_zona                  BIGINT                                            NOT NULL,
   id_barrio                BIGINT                                            NOT NULL,
   id_cliente               SERIAL                                            NOT NULL,
   cod_cliente              VARCHAR                                           NOT NULL,
   nom_cliente              VARCHAR                                           NOT NULL,
   cel_cliente              VARCHAR                                           NOT NULL,
   mail_cliente             VARCHAR                                           NOT NULL,
   fec_aniversario          DATE DEFAULT 'today'                              NOT NULL,
   val_login                VARCHAR                                           NOT NULL,
   val_passwd               VARCHAR                                           NOT NULL,
   val_passwd_encrypted     VARCHAR                                           NOT NULL,
   val_latitud              DECIMAL(10,6) DEFAULT 0.0                         NOT NULL,
   val_longitud             DECIMAL(10,6) DEFAULT 0.0                         NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_cliente),
  FOREIGN KEY(id_pais) REFERENCES tab_paises(id_pais),
  FOREIGN KEY(id_departamento) REFERENCES tab_departamentos(id_departamento),
  FOREIGN KEY(id_ciudad) REFERENCES tab_ciudades(id_ciudad),
  FOREIGN KEY(id_zona) REFERENCES tab_zonas(id_zona),
  FOREIGN KEY(id_barrio) REFERENCES tab_barrios(id_barrio)
);
-- CREATE TABLE tab_paquetes(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_paquetes_caracteristicas(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_paquetes_servicios(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_paquetes_productos(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_paquetes_habitaciones(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_head_promociones(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_deta_promociones(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_hospedajes_promociones(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
CREATE TABLE tab_head_reservacion(
-- ********************     ********************************************      *********
   id_reservacion           SERIAL                                            NOT NULL,
   cod_reservacion          VARCHAR                                           NOT NULL,
   fec_reservacion          DATE DEFAULT 'today'                              NOT NULL,
   hora_inicio              TIME DEFAULT 'now()'                              NOT NULL,
   hora_final               TIME DEFAULT 'now()'                              NOT NULL,
   id_cliente               BIGINT                                            NOT NULL,
   id_hospedaje             BIGINT                                            NOT NULL,
   id_habitacion            BIGINT                                            NOT NULL,
   num_personas_extras      INTEGER DEFAULT 0                                 NOT NULL,
   val_precio_neto          DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   val_precio_total         DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_reservacion),
  FOREIGN KEY(id_cliente) REFERENCES tab_clientes(id_cliente),
  FOREIGN KEY(id_hospedaje) REFERENCES tab_hospedajes(id_hospedaje),
  FOREIGN KEY(id_habitacion) REFERENCES tab_habitaciones(id_habitacion)
);
CREATE TABLE tab_deta_productos_reservacion(
-- ********************     ********************************************      *********
   id_reservacion           BIGINT                                            NOT NULL,
   id_lista_precio          BIGINT                                            NOT NULL,
   val_cantidad             INTEGER                                           NOT NULL,
   val_precio               DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_reservacion, id_lista_precio),
  FOREIGN KEY(id_reservacion) REFERENCES tab_head_reservacion(id_reservacion),
  FOREIGN KEY(id_lista_precio) REFERENCES tab_hospedajes_listas_precios(id_lista_precio)
);
CREATE TABLE tab_deta_servicios_reservacion(
-- ********************     ********************************************      *********
   id_reservacion           BIGINT                                            NOT NULL,
   id_servicio              BIGINT                                            NOT NULL,
   val_cantidad             INTEGER                                           NOT NULL,
   val_precio               DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_reservacion, id_servicio),
  FOREIGN KEY(id_reservacion) REFERENCES tab_head_reservacion(id_reservacion),
  FOREIGN KEY(id_servicio) REFERENCES tab_servicios(id_servicio)
);
CREATE TABLE tab_deta_caracteristicas_reservacion(
-- ********************     ********************************************      *********
   id_reservacion           BIGINT                                            NOT NULL,
   id_caracteristica        BIGINT                                            NOT NULL,
   val_cantidad             INTEGER                                           NOT NULL,
   val_precio               DECIMAL(10,2) DEFAULT 0.00                        NOT NULL,
   ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
   ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
   val_comentario           VARCHAR DEFAULT 'Sin comentario!',
   usr_insert               VARCHAR                                           NOT NULL,
   fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
   usr_update               VARCHAR,
   fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
  PRIMARY KEY(id_reservacion, id_caracteristica),
  FOREIGN KEY(id_reservacion) REFERENCES tab_head_reservacion(id_reservacion),
  FOREIGN KEY(id_caracteristica) REFERENCES tab_caracteristicas(id_caracteristica)
);
-- CREATE TABLE tab_head_factura(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_deta_factura(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_reportes(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_head_mensajes(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_deta_mensajes(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_monitoreo(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_auditoria_registros(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_auditoria_borrado(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_facturacion(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );
-- CREATE TABLE tab_temporal(
-- -- ********************     ********************************************      *********
--    ind_bloqueo              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    ind_borrado              BOOLEAN DEFAULT FALSE                             NOT NULL,
--    val_comentario           VARCHAR DEFAULT 'Sin comentario!',
--    usr_insert               VARCHAR                                           NOT NULL,
--    fec_insert               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()'       NOT NULL,
--    usr_update               VARCHAR,
--    fec_update               TIMESTAMP WITHOUT TIME ZONE DEFAULT 'now()',
-- );

CREATE OR REPLACE FUNCTION fun_act_general() RETURNS "trigger" AS $$
BEGIN
   IF TG_OP = 'INSERT' THEN
      IF NEW.usr_insert IS NULL THEN
         NEW.usr_insert = current_user;
      END IF;
      NEW.fec_insert = current_timestamp;
   END IF;
   IF TG_OP = 'UPDATE' THEN
      IF NEW.usr_update IS NULL THEN
         NEW.usr_update = current_user;
      END IF;
      NEW.fec_update = current_timestamp;
   END IF;
   RETURN NEW;
END;
$$     LANGUAGE plpgsql;

CREATE FUNCTION fun_gen_general_codigos_registros() RETURNS "trigger" AS $$
  DECLARE wcod_registro VARCHAR;
  DECLARE wval_year VARCHAR;
  DECLARE wval_month VARCHAR;
  DECLARE wval_day VARCHAR;
  DECLARE wid_registro BIGINT;
BEGIN
   wval_year = extract('year' from current_timestamp)::VARCHAR;
   wval_month = extract('month' from current_timestamp)::VARCHAR;
   wval_day = extract('day' from current_timestamp)::VARCHAR;
   wcod_registro = wval_year || wval_month || wval_day || '-';
   --  tab_pmtros - TABLA PMTROS
   IF TG_TABLE_NAME = 'tab_pmtros' THEN
      SELECT MAX(id_pmtro) INTO wid_registro FROM tab_pmtros;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_pmtro = 'PMT' || wcod_registro || wid_registro;
   END IF;
   --  tab_roles                             - TABLA ROLES
   IF TG_TABLE_NAME = 'tab_roles' THEN
      SELECT MAX(id_role) INTO wid_registro FROM tab_roles;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_role = 'ROL' || wcod_registro || wid_registro;
   END IF;
   --  tab_info_empresa                      - TABLA INFORMACION EMPRESA
   IF TG_TABLE_NAME = 'tab_info_empresa' THEN
      SELECT MAX(id_empresa) INTO wid_registro FROM tab_info_empresa;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_empresa = 'IEM' || wcod_registro || wid_registro;
   END IF;
   --  tab_bancos_cuentas                    - TABLA BANCOS CUENTAS
   IF TG_TABLE_NAME = 'tab_bancos_cuentas' THEN
      SELECT MAX(id_cuenta) INTO wid_registro FROM tab_bancos_cuentas;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_cuenta = 'BCU' || wcod_registro || wid_registro;
   END IF;
   --  tab_paises                            - TABLA PAISES
   IF TG_TABLE_NAME = 'tab_paises' THEN
      SELECT MAX(id_pais) INTO wid_registro FROM tab_paises;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_pais = 'PAI' || wcod_registro || wid_registro;
   END IF;
   --  tab_departamentos                     - TABLA DEPARTAMENTOS
   IF TG_TABLE_NAME = 'tab_departamentos' THEN
      SELECT MAX(id_departamento) INTO wid_registro FROM tab_departamentos;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_departamento = 'DEP' || wcod_registro || wid_registro;
   END IF;
   --  tab_ciudades                          - TABLA CIUDADES
   IF TG_TABLE_NAME = 'tab_ciudades' THEN
      SELECT MAX(id_ciudad) INTO wid_registro FROM tab_ciudades;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_ciudad = 'CIU' || wcod_registro || wid_registro;
   END IF;
   --  tab_zonas                             - TABLA ZONAS
   IF TG_TABLE_NAME = 'tab_zonas' THEN
      SELECT MAX(id_zona) INTO wid_registro FROM tab_zonas;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_zona = 'ZON' || wcod_registro || wid_registro;
   END IF;
   --  tab_barrios                           - TABLA BARRIOS
   IF TG_TABLE_NAME = 'tab_barrios' THEN
      SELECT MAX(id_barrio) INTO wid_registro FROM tab_barrios;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_barrio = 'BAR' || wcod_registro || wid_registro;
   END IF;
   --  tab_direcciones                       - TABLA DIRECCIONES
   IF TG_TABLE_NAME = 'tab_direcciones' THEN
      SELECT MAX(id_direccion) INTO wid_registro FROM tab_direcciones;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_direccion = 'DIR' || wcod_registro || wid_registro;
   END IF;
   --  tab_meses                             - TABLA MESES
   IF TG_TABLE_NAME = 'tab_meses' THEN
      SELECT MAX(id_mes) INTO wid_registro FROM tab_meses;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_mes = 'MES' || wcod_registro || wid_registro;
   END IF;
   --  tab_festivos                          - TABLA MESES
   IF TG_TABLE_NAME = 'tab_festivos' THEN
      SELECT MAX(id_festivo) INTO wid_registro FROM tab_festivos;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_festivo = 'FES' || wcod_registro || wid_registro;
   END IF;
   --  tab_novedades                         - TABLA NOVEDADES
   IF TG_TABLE_NAME = 'tab_novedades' THEN
      SELECT MAX(id_novedad) INTO wid_registro FROM tab_novedades;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_novedad = 'NOV' || wcod_registro || wid_registro;
   END IF;
   --  tab_motivos                           - TABLA MOTIVOS
   IF TG_TABLE_NAME = 'tab_motivos' THEN
      SELECT MAX(id_motivo) INTO wid_registro FROM tab_motivos;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_motivo = 'MOT' || wcod_registro || wid_registro;
   END IF;
   --  tab_eventos                           - TABLA EVENTOS
   IF TG_TABLE_NAME = 'tab_eventos' THEN
      SELECT MAX(id_evento) INTO wid_registro FROM tab_eventos;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_evento = 'EVE' || wcod_registro || wid_registro;
   END IF;
   --  tab_alertas                           - TABLA ALERTAS
   IF TG_TABLE_NAME = 'tab_alertas' THEN
      SELECT MAX(id_alerta) INTO wid_registro FROM tab_alertas;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_alerta = 'ALE' || wcod_registro || wid_registro;
   END IF;
   --  tab_estados                           - TABLA ESTADOS
   IF TG_TABLE_NAME = 'tab_estados' THEN
      SELECT MAX(id_estado) INTO wid_registro FROM tab_estados;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_estado = 'EST' || wcod_registro || wid_registro;
   END IF;
   --  tab_marcas                            - TABLA MARCAS PRODUCTO
   IF TG_TABLE_NAME = 'tab_marcas' THEN
      SELECT MAX(id_marca) INTO wid_registro FROM tab_marcas;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_marca = 'MAR' || wcod_registro || wid_registro;
   END IF;
   --  tab_lineas                            - TABLA LINEAS PRODUCTO
   IF TG_TABLE_NAME = 'tab_lineas' THEN
      SELECT MAX(id_linea) INTO wid_registro FROM tab_lineas;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_linea = 'LIN' || wcod_registro || wid_registro;
   END IF;
   --  tab_sublineas                         - TABLA SUBLINEAS PRODUCTO
   IF TG_TABLE_NAME = 'tab_sublineas' THEN
      SELECT MAX(id_sublinea) INTO wid_registro FROM tab_sublineas;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_sublinea = 'SUB' || wcod_registro || wid_registro;
   END IF;
   --  tab_productos                         - TABLA PRODUCTOS
   IF TG_TABLE_NAME = 'tab_productos' THEN
      SELECT MAX(id_producto) INTO wid_registro FROM tab_productos;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_producto = 'PRO' || wcod_registro || wid_registro;
   END IF;
   --  tab_tipos_hospedajes                  - TABLA TIPOS DE HOSPEDAJE
   IF TG_TABLE_NAME = 'tab_tipos_hospedajes' THEN
      SELECT MAX(id_tipo_hospedaje) INTO wid_registro FROM tab_tipos_hospedajes;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_tipo_hospedaje = 'THO' || wcod_registro || wid_registro;
   END IF;
   --  tab_hospedajes                        - TABLA HOSPEDAJE
   IF TG_TABLE_NAME = 'tab_hospedajes' THEN
      SELECT MAX(id_hospedaje) INTO wid_registro FROM tab_hospedajes;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_hospedaje = 'HOS' || wcod_registro || wid_registro;
   END IF;
   --  tab_hospedajes_listas_precios         - TABLA LISTA DE PRECIOS DE PRODUCTOS
   IF TG_TABLE_NAME = 'tab_hospedajes_listas_precios' THEN
      SELECT MAX(id_lista_precio) INTO wid_registro FROM tab_hospedajes_listas_precios;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_lista_precio = 'HLP' || wcod_registro || wid_registro;
   END IF;
   --  tab_hospedajes_horarios               - TABLA HORARIO DE HOSPEDAJES
   IF TG_TABLE_NAME = 'tab_hospedajes_horarios' THEN
      SELECT MAX(id_horario) INTO wid_registro FROM tab_hospedajes_horarios;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_horario = 'HHO' || wcod_registro || wid_registro;
   END IF;
   --  tab_hospedajes_usuarios               - TABLA USUARIO (EMPLEADOS) DE HOSPEDAJES
   IF TG_TABLE_NAME = 'tab_hospedajes_usuarios' THEN
      SELECT MAX(id_usuario) INTO wid_registro FROM tab_hospedajes_usuarios;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_usuario = 'HUS' || wcod_registro || wid_registro;
   END IF;
   --  tab_tipos_habitaciones                - TABLA TIPOS DE HABITACIONES
   IF TG_TABLE_NAME = 'tab_tipos_habitaciones' THEN
      SELECT MAX(id_tipo_habitacion) INTO wid_registro FROM tab_tipos_habitaciones;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_tipo_habitacion = 'THA' || wcod_registro || wid_registro;
   END IF;
   --  tab_habitaciones                      - TABLA HABITACIONES
   IF TG_TABLE_NAME = 'tab_habitaciones' THEN
      SELECT MAX(id_habitacion) INTO wid_registro FROM tab_habitaciones;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_habitacion = 'HAB' || wcod_registro || wid_registro;
   END IF;
   --  tab_tipos_servicios                   - TABLA TIPOS DE SERVICIOS
   IF TG_TABLE_NAME = 'tab_tipos_servicios' THEN
      SELECT MAX(id_tipo_servicio) INTO wid_registro FROM tab_tipos_servicios;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_tipo_servicio = 'TSE' || wcod_registro || wid_registro;
   END IF;
   --  tab_servicios                         - TABLA SERVICIOS
   IF TG_TABLE_NAME = 'tab_servicios' THEN
      SELECT MAX(id_servicio) INTO wid_registro FROM tab_pmtros;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_servicio = 'SER' || wcod_registro || wid_registro;
   END IF;
   --  tab_tipos_caracteristicas             - TABLA TIPOS DE CARACTERISTICAS
   IF TG_TABLE_NAME = 'tab_tipos_caracteristicas' THEN
      SELECT MAX(id_tipo_caracteristica) INTO wid_registro FROM tab_tipos_caracteristicas;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_tipo_caracteristica = 'TCA' || wcod_registro || wid_registro;
   END IF;
   --  tab_caracteristicas                   - TABLA CARACTERISTICAS
   IF TG_TABLE_NAME = 'tab_caracteristicas' THEN
      SELECT MAX(id_caracteristica) INTO wid_registro FROM tab_caracteristicas;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_caracteristica = 'CAR' || wcod_registro || wid_registro;
   END IF;
   --  tab_clientes                          - TABLA CLIENTES
   IF TG_TABLE_NAME = 'tab_clientes' THEN
      SELECT MAX(id_cliente) INTO wid_registro FROM tab_clientes;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_cliente = 'CLI' || wcod_registro || wid_registro;
   END IF;
   --  tab_paquetes                          - TABLA PAQUETES
   --  tab_paquetes_caracteristicas          - TABLA CARACTERISTICAS INCLUIDAS EN EL PAQUETE
   --  tab_paquetes_servicios                - TABLA SERVICIOS INCLUIDOS EN EL PAQUETE
   --  tab_paquetes_productos                - TABLA PRODUCTOS INCLUIDOS EN EL PAQUETE
   --  tab_paquetes_habitaciones             - TABLA HABITACION DEL PAQUETE
   --  tab_head_promociones                  - TABLA PROMOCIONES
   --  tab_deta_promociones                  - TABLA DETALLES DE LAS PROMOCIONES
   --  tab_hospedajes_promociones            - TABLA PROMOCIONES DE LOS HOSPEDAJES
   --  tab_head_reservacion                  - TABLA RESERVACIONES
   IF TG_TABLE_NAME = 'tab_head_reservacion' THEN
      SELECT MAX(id_reservacion) INTO wid_registro FROM tab_head_reservacion;
      IF wid_registro IS NULL THEN
         wid_registro = 0;
      END IF;
      wid_registro = wid_registro + 1;
      NEW.cod_reservacion = 'HRE' || wcod_registro || wid_registro;
   END IF;
   --  tab_head_factura                      - TABLA FACTURAS
   --  tab_deta_factura                      - TABLA DETALLE DE LAS FACTURAS
   --  tab_reportes                          - TABLA REPORTES
   --  tab_head_mensajes                     - TABLA MENSAJES
   --  tab_deta_mensajes                     - TABLA DETALE DE LOS MENSAJES
   --  tab_monitoreo                         - TABLA MONITOREO
   --  tab_auditoria_registros               - TABLA AUDITORIA DE REGISTROS
   --  tab_auditoria_borrado                 - TABLA AUDITORIA DE BORRADO
   --  tab_facturacion                       - TABLA FACTURACION
   --  tab_temporal                          - TABLA TEMPORAL
  RETURN NEW;
END;
$$     LANGUAGE plpgsql;

CREATE FUNCTION fun_gen_encrypted_passwd() RETURNS "trigger" AS $$
BEGIN
  NEW.val_passwd_encrypted = md5(NEW.val_passwd);
  RETURN NEW;
END;
$$     LANGUAGE plpgsql;

CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_pmtros FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_roles FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_info_empresa FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_bancos_cuentas FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_paises FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_departamentos FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_ciudades FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_zonas FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_barrios FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_direcciones FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_meses FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_festivos FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_novedades FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_motivos FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_eventos FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_alertas FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_estados FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_marcas FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_lineas FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_sublineas FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_productos FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_tipos_hospedajes FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_hospedajes FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_hospedajes_listas_precios FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_hospedajes_horarios FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_hospedajes_usuarios FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_tipos_habitaciones FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_habitaciones FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_tipos_servicios FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_servicios FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_tipos_caracteristicas FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_caracteristicas FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_hospedajes_habitaciones FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_hospedajes_servicios FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_hospedajes_caracteristicas FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_habitaciones_servicios FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_habitaciones_caracteristicas FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_clientes FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_paquetes FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_paquetes_caracteristicas FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_paquetes_servicios FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_paquetes_productos FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_paquetes_habitaciones FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_head_promociones FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_deta_promociones FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_hospedajes_promociones FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_head_reservacion FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_deta_productos_reservacion FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_deta_servicios_reservacion FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_deta_caracteristicas_reservacion FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_head_factura FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_deta_factura FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_reportes FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_head_mensajes FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_deta_mensajes FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_monitoreo FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_auditoria_registros FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_auditoria_borrado FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_facturacion FOR EACH ROW EXECUTE PROCEDURE fun_act_general();
-- CREATE TRIGGER tri_act_general BEFORE INSERT OR UPDATE ON tab_temporal FOR EACH ROW EXECUTE PROCEDURE fun_act_general();

CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_pmtros FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_roles FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_info_empresa FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_bancos_cuentas FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_paises FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_departamentos FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_ciudades FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_zonas FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_barrios FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_direcciones FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_meses FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_festivos FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_novedades FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_motivos FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_eventos FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_alertas FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_estados FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_marcas FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_lineas FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_sublineas FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_productos FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_tipos_hospedajes FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_hospedajes FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_hospedajes_listas_precios FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_hospedajes_horarios FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_hospedajes_usuarios FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_tipos_habitaciones FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_habitaciones FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_tipos_servicios FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_servicios FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_tipos_caracteristicas FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_caracteristicas FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_clientes FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_paquetes FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_paquetes_caracteristicas FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_paquetes_servicios FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_paquetes_productos FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_paquetes_habitaciones FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_head_promociones FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_deta_promociones FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_hospedajes_promociones FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_head_reservacion FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_deta_productos_reservacion FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_deta_servicios_reservacion FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_deta_caracteristicas_reservacion FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_head_factura FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_deta_factura FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_reportes FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_head_mensajes FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_deta_mensajes FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_monitoreo FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_auditoria_registros FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_auditoria_borrado FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_facturacion FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();
-- CREATE TRIGGER tri_gen_general_codigos_registros BEFORE INSERT ON tab_temporal FOR EACH ROW EXECUTE PROCEDURE fun_gen_general_codigos_registros();

CREATE TRIGGER tri_gen_encrypted_passwd BEFORE INSERT OR UPDATE ON tab_hospedajes_usuarios FOR EACH ROW EXECUTE PROCEDURE fun_gen_encrypted_passwd();
CREATE TRIGGER tri_gen_encrypted_passwd BEFORE INSERT OR UPDATE ON tab_clientes FOR EACH ROW EXECUTE PROCEDURE fun_gen_encrypted_passwd();